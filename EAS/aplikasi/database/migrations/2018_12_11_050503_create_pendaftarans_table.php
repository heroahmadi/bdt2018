<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftarans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('no_un')->unique();
            $table->integer('bind');
            $table->integer('big');
            $table->integer('mat');
            $table->integer('ipa');
            $table->integer('total');
            $table->string('nama');
            $table->string('asal_sekolah');
            $table->string('nama_ortu');
            $table->integer('pilihan1');
            $table->integer('pilihan2');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftarans');
    }
}
