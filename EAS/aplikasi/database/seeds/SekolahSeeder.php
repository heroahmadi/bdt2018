<?php

use Illuminate\Database\Seeder;
use App\Sekolah;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [];
        for ($i=1; $i < 20; $i++) 
        { 
            $sekolah = [
                'id' => $i,
                'nama_sekolah' => 'SMA Negeri '.$i
            ];
            $data[] = $sekolah;
        }

        Sekolah::insert($data);
    }
}
