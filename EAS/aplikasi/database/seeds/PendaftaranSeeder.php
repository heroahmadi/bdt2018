<?php

use Illuminate\Database\Seeder;
use App\Pendaftaran;

class PendaftaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $count = 1;
        for ($i=1; $i < 20 ; $i++) { 
            for ($j=1; $j <= 120 ; $j++) { 
                $pendaftaran = new Pendaftaran;
                $pendaftaran->no_un = 'UN_'.$i.'_'.$j;
                $pendaftaran->nama = 'NAMA_SISWA_'.$count;
                $pendaftaran->asal_sekolah = 'SMPN '.$i;
                $pendaftaran->nama_ortu = 'default';
                $pendaftaran->bind = rand(0, 100);
                $pendaftaran->big = rand(0, 100); 
                $pendaftaran->mat = rand(0, 100); 
                $pendaftaran->ipa = rand(0, 100); 
                $pendaftaran->total = $pendaftaran->bind + $pendaftaran->big + $pendaftaran->mat + $pendaftaran->ipa;
                $pendaftaran->pilihan1 = $i;
                $pendaftaran->pilihan2 = ($i % 19) + 1;
                $pendaftaran->save();

                $count++;
            }
        }
    }
}
