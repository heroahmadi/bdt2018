<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PendaftaranController@index');
Route::post('/', 'PendaftaranController@daftar');
Route::get('/hasil', 'HasilController@index');
Route::get('/hasil/filter', 'HasilController@rank');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => 'auth'], function(){
    Route::get('/pendaftaran/edit/{id}', 'PendaftaranController@edit');
    Route::post('/pendaftaran/edit/{id}', 'PendaftaranController@update');
    Route::get('/pendaftaran/delete/{id}', 'PendaftaranController@destroy');
});
