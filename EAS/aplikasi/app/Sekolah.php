<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sekolah extends Model
{
    public function pilihan1()
    {
        return $this->hasMany('App\Pendaftaran', 'pilihan1');
    }

    public function pilihan2()
    {
        return $this->hasMany('App\Pendaftaran', 'pilihan2');
    }

    public function pendaftaran()
    {
        return $this->pilihan1->merge($this->pilihan2);
    }
}
