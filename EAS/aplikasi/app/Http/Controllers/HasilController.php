<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redis;
use App\Pendaftaran;
use App\Sekolah;
use App\RedisClient;
use GuzzleHttp\Client;

class HasilController extends Controller
{
    public function index(Request $request)
    {
        if($request->sekolah_id)
            $sekolah_id = $request->sekolah_id;
        else
            $sekolah_id = 1;

        $sekolah = Sekolah::all();
        $pendaftaran = $this->rank($sekolah_id);

        $data = [
            'pendaftaran' => $pendaftaran,
            'sekolah' => $sekolah
        ];

        return view('pages.hasil', $data);
    }

    public function rank($sekolah_id)
    {
        $sekolah = Sekolah::findOrFail($sekolah_id);

        $client = new Client();
        $pendaftaran = $client->get('localhost:5000/'.$sekolah->id)->getBody();
        $pendaftaran = json_decode($pendaftaran);
        
        if($pendaftaran)
            return $pendaftaran;
        
        $pendaftaran = Pendaftaran::where('pilihan1', $sekolah->id)->orWhere('pilihan2', $sekolah->id)->orderBy('total', 'desc')->orderBy('created_at', 'asc')->get();
        // dd($pendaftaran);

        if(count($pendaftaran))
        {
            $rank = 1;
            foreach ($pendaftaran as $item)
            {
                $item->rank = $rank++;
                $item->pilihan1 = $item->sekolah_pilihan1->nama_sekolah;
                $item->pilihan2 = $item->sekolah_pilihan2->nama_sekolah;
            }
    
            $result = $client->post('localhost:5000/'.$sekolah->id, [
                'json' => [
                    $pendaftaran->toJson()
                ]
            ]);
        }

        return $pendaftaran;
    }
}
