<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Sekolah;
use App\Pendaftaran;

class PendaftaranController extends Controller
{
    public function index()
    {
        $data['sekolah'] = Sekolah::all();
        return view('pages.pendaftaran', $data);
    }

    public function daftar(Request $request)
    {
        $data = [
            'no_un' => $request->input('no_un'),
            'bind' => $request->input('bind'),
            'big' => $request->input('bing'),
            'mat' => $request->input('mat'),
            'ipa' => $request->input('ipa'),
            'total' => $request->input('bind') + $request->input('bing') + $request->input('mat') + $request->input('ipa'),
            'nama' => $request->input('nama'),
            'asal_sekolah' => $request->input('asal_sekolah'),
            'nama_ortu' => $request->input('ortu'),
            'pilihan1' => $request->input('pilihan1'),
            'pilihan2' => $request->input('pilihan2')
        ];

        $pendaftaran = Pendaftaran::create($data);

        $status = 1;
        $title = 'Berhasil!';
        $message = 'Pendaftaran Berhasil!';
        return redirect('/hasil')
                ->with('status', $status)
                ->with('title', $title)
                ->with('message', $message);
    }

    public function edit($id)
    {
        $pendaftaran = Pendaftaran::findOrFail($id);
        $sekolah = Sekolah::all();

        return view('edit', compact('pendaftaran', 'sekolah'));
    }

    public function update(Request $request)
    {
        $pendaftaran = Pendaftaran::findOrFail($request->input('id'));
        $pendaftaran->no_un = $request->input('no_un');
        $pendaftaran->nama = $request->input('nama');
        $pendaftaran->bind = $request->input('bind');
        $pendaftaran->big = $request->input('big');
        $pendaftaran->mat = $request->input('mat');
        $pendaftaran->ipa = $request->input('ipa');
        $pendaftaran->total = $request->input('bind') + $request->input('big') + $request->input('mat') + $request->input('ipa');
        $pendaftaran->pilihan1 = $request->input('pilihan1');
        $pendaftaran->pilihan2 = $request->input('pilihan2');
        $pendaftaran->save();

        $status = 1;
        $title = 'Berhasil!';
        $message = 'Edit Berhasil!';
        return redirect('/home')
                ->with('status', $status)
                ->with('title', $title)
                ->with('message', $message);
    }

    public function destroy($id)
    {
        $pendaftaran = Pendaftaran::findOrFail($id);
        $pendaftaran->delete();

        $status = 1;
        $title = 'Berhasil!';
        $message = 'Hapus Berhasil!';
        return redirect('/home')
                ->with('status', $status)
                ->with('title', $title)
                ->with('message', $message);
    }
}
