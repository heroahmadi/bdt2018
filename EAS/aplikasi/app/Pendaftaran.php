<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    protected $fillable = [
        'id', 'no_un', 'bind', 'big', 'mat', 'ipa', 'nama', 'asal_sekolah', 'nama_ortu', 'pilihan1', 'pilihan2', 'total'
    ];
    
    public function sekolah_pilihan1()
    {
        return $this->belongsTo('App\Sekolah', 'pilihan1');
    }

    public function sekolah_pilihan2()
    {
        return $this->belongsTo('App\Sekolah', 'pilihan2');
    }
}
