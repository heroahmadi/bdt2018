@extends('template')

@section('content')
<div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
        <form role="form" action="" method="POST">
            @csrf
            <h2>Pendaftaran <small>Penerimaan Peserta Didik Baru 2019 SMA</small></h2>
            <hr class="colorgraph">
            <h3>Data Pribadi</h3>
            <div class="form-group">
                <label>Nama Lengkap</label>
                <input type="text" name="nama" id="nama" class="form-control input-lg" placeholder="Nama Lengkap"
                    tabindex="3">
            </div>
            <div class="form-group">
                <label>Asal Sekolah</label>
                <input type="text" name="asal_sekolah" id="asal_sekolah" class="form-control input-lg" placeholder="Asal Sekolah"
                    tabindex="3">            
            </div>
            <div class="form-group">
                <label>Nama Orang Tua</label>
                <input type="text" name="ortu" id="ortu" class="form-control input-lg" placeholder="Nama Orang Tua"
                    tabindex="3">
            </div>
            <hr class="colorgraph">
            <h3>Data UN</h3>
            <div class="form-group">
                <label>No UN</label>
                <input type="text" name="no_un" id="no_un" class="form-control input-lg" placeholder="No UN"
                    tabindex="3">
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Bhs Indonesia</label>
                        <input type="text" name="bind" id="bind" class="form-control input-lg" placeholder="Nilai Bhs. Indonesia"
                            tabindex="3">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Bhs Inggris</label>
                        <input type="text" name="bing" id="bing" class="form-control input-lg" placeholder="Nilai Bhs. Inggris"
                            tabindex="3">
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Matematika</label>
                        <input type="text" name="mat" id="mat" class="form-control input-lg" placeholder="Nilai Matematika"
                            tabindex="3">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label>IPA</label>
                        <input type="text" name="ipa" id="ipa" class="form-control input-lg" placeholder="Nilai IPA"
                            tabindex="3">
                    </div>
                </div>
            </div>
            <hr class="colorgraph">
            <h3>Pilihan Pendaftaran</h3>
            <div class="form-group">
                <label>Pilihan 1</label>
                <select name="pilihan1" id="" class="form-control">
                    <option selected disabled>Pilih Sekolah</option>
                    @foreach ($sekolah as $sek)
                    <option value="{{ $sek->id }}">{{ $sek->nama_sekolah }}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Pilihan 2</label>
                <select name="pilihan2" id="" class="form-control">
                    <option selected disabled>Pilih Sekolah</option>
                    @foreach ($sekolah as $sek)
                    <option value="{{ $sek->id }}">{{ $sek->nama_sekolah }}</option>
                    @endforeach
                </select>
            </div>

            <div class="row">
                <div class="col-xs-12 col-md-12"><button type="submit" class="btn btn-success btn-block btn-lg">Daftar</button></div>
            </div>
        </form>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="t_and_c_m" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h4 class="modal-title" id="myModalLabel">Terms & Conditions</h4>
            </div>
            <div class="modal-body">
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Similique, itaque, modi, aliquam
                    nostrum at sapiente consequuntur natus odio reiciendis perferendis rem nisi tempore possimus
                    ipsa porro delectus quidem dolorem ad.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">I Agree</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
@endsection
