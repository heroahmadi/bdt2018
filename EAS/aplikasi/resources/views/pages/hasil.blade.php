@extends('template')

@section('content')
    <div class="container">
        <h1>Ranking Sementara PPDB 2019</h1>
        <div class="row">
            <div class="col-md-4">
                <label>Filter Sekolah</label>
                <form action="" method="GET" id="change_sekolah">
                    <select id="sekolah" name="sekolah_id" class="form-control">
                        @foreach ($sekolah as $sek)
                            <option value="{{ $sek->id }}" {{ request('sekolah_id') == $sek->id ? 'selected' : '' }}>{{ $sek->nama_sekolah }}</option>
                        @endforeach
                    </select>
                </form>
            </div>
            <div class="col-md-8">
                <div class="text-right">
                    <p><b>Update Terakhir:</b> {{ \Carbon\Carbon::now('Asia/Jakarta')->format('d F H:i') }}:00 WIB</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table class="table table-hover table-stripped" id="table">
                    <thead>
                        <tr>
                            <th>Rank</th>
                            <th>No UN</th>
                            <th>Nama Siswa</th>
                            <th>BIND</th>
                            <th>BIG</th>
                            <th>MAT</th>
                            <th>IPA</th>
                            <th>Total</th>
                            <th>Pilihan 1</th>
                            <th>Pilihan 2</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach ($pendaftaran as $daftar)
                            <tr>
                                <td>{{ $daftar->rank }}</td>
                                <td>{{ $daftar->no_un }}</td>
                                <td>{{ $daftar->nama }}</td>
                                <td>{{ $daftar->bind }}</td>
                                <td>{{ $daftar->big }}</td>
                                <td>{{ $daftar->mat }}</td>
                                <td>{{ $daftar->ipa }}</td>
                                <td>{{ $daftar->total }}</td>
                                <td>{{ $daftar->pilihan1 }}</td>
                                <td>{{ $daftar->pilihan2 }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    {{-- <script>
        $(document).ready(function(){
            $("#sekolah").change();
        });

        $("#sekolah").change(function(){
            $.ajax({
                url: '{{ url('/hasil/filter') }}',
                type: 'GET',
                data: {
                    'sekolah_id' : $(this).val()
                },
                success: function(data){
                    $("#tbody").empty();
                    $.each(data, function(ind, val){
                        var element = `
                            <tr>
                                <td>`+(ind+1)+`</td>
                                <td>`+val.no_un+`</td>
                                <td>`+val.nama+`</td>
                                <td>`+val.bind+`</td>
                                <td>`+val.big+`</td>
                                <td>`+val.mat+`</td>
                                <td>`+val.ipa+`</td>
                                <td>`+val.total+`</td>
                                <td>`+val.pilihan1+`</td>
                                <td>`+val.pilihan2+`</td>
                            </tr>
                        `;

                        $("#tbody").append(element);
                    });
                }
            });
        });
    </script> --}}
    <script>
        $("#table").DataTable({
            "order": [[ 0, "asc" ]],
            "columnDefs": [
                {orderable: false, targets: [1,2,3,4,5,6,7,8,9]}
            ]
        });

        $("#sekolah").change(function(){
            $("#change_sekolah").submit();
        });
    </script>
@endsection