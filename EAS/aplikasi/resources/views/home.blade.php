@extends('layouts.app')

@section('custom-css')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/datatables.min.css"/>
@endsection

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Dashboard</h2>
                </div>

                <div class="card-body">
                    <table class="table table-stripped" id="table" width="100%">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>No UN</th>
                                <th>Nama Siswa</th>
                                <th>BIND</th>
                                <th>BIG</th>
                                <th>MAT</th>
                                <th>IPA</th>
                                <th>Total</th>
                                <th>Pilihan 1</th>
                                <th>Pilihan 2</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $i = 1;
                            @endphp
                            @foreach ($pendaftaran as $daftar)
                                <tr>
                                    <td>{{ $i++ }}</td>
                                    <td>{{ $daftar->no_un }}</td>
                                    <td>{{ $daftar->nama }}</td>
                                    <td>{{ $daftar->bind }}</td>
                                    <td>{{ $daftar->big }}</td>
                                    <td>{{ $daftar->mat }}</td>
                                    <td>{{ $daftar->ipa }}</td>
                                    <td>{{ $daftar->total }}</td>
                                    <td>{{ $daftar->sekolah_pilihan1->nama_sekolah }}</td>
                                    <td>{{ $daftar->sekolah_pilihan2->nama_sekolah }}</td>
                                    <td>
                                        <a href="{{ url('pendaftaran/edit/'.$daftar->id) }}" class="btn btn-sm btn-primary">Edit</a>
                                        <a href="{{ url('pendaftaran/delete/'.$daftar->id) }}" class="btn btn-sm btn-danger">Hapus</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection


@section('custom-js')
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    {{-- <script src="{{ asset('datatables/datatables.min.js') }}"></script> --}}
    <script>
        // $('#table').DataTable();
    </script>
@endsection