@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Edit</h2>
                </div>

                <div class="card-body">
                    <form action="" method="post">
                        @csrf

                        <input type="hidden" name="id" value="{{ $pendaftaran->id }}">
                        <div class="form-group">
                            <label>No UN</label>
                            <input type="text" name="no_un" class="form-control" value="{{ $pendaftaran->no_un }}">
                        </div>

                        <div class="form-group">
                            <label>Nama Siswa</label>
                            <input type="text" name="nama" class="form-control" value="{{ $pendaftaran->nama }}">
                        </div>

                        <div class="form-group">
                            <label>BIND</label>
                            <input type="number" name="bind" class="form-control" value="{{ $pendaftaran->bind }}">
                        </div>

                        <div class="form-group">
                            <label>BIG</label>
                            <input type="number" name="big" class="form-control" value="{{ $pendaftaran->big }}">
                        </div>
                        
                        <div class="form-group">
                            <label>Mat</label>
                            <input type="number" name="mat" class="form-control" value="{{ $pendaftaran->mat }}">
                        </div>

                        <div class="form-group">
                            <label>IPA</label>
                            <input type="number" name="ipa" class="form-control" value="{{ $pendaftaran->ipa }}">
                        </div>

                        <div class="form-group">
                            <label>Pilihan 1</label>
                            <select name="pilihan1" id="" class="form-control">
                                <option selected disabled>Pilih Sekolah</option>
                                @foreach ($sekolah as $sek)
                                <option value="{{ $sek->id }}" {{ $sek->id == $pendaftaran->pilihan1 ? 'selected' : '' }} >{{ $sek->nama_sekolah }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pilihan 2</label>
                            <select name="pilihan2" id="" class="form-control">
                                <option selected disabled>Pilih Sekolah</option>
                                @foreach ($sekolah as $sek)
                                <option value="{{ $sek->id }}" {{ $sek->id == $pendaftaran->pilihan2 ? 'selected' : '' }} >{{ $sek->nama_sekolah }}</option>
                                @endforeach
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success">Edit</button>
                    </form>
                </div>
            </div>            
        </div>
    </div>
</div>
@endsection