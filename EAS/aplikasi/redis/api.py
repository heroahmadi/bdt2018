from flask import Flask, request
from json import dumps, loads
from rediscluster import StrictRedisCluster

app = Flask(__name__)

nodes = [{"host": "192.168.34.11", "port": "7001"}, {"host": "192.168.34.12", "port": "7001"}, {"host": "192.168.34.13", "port": "7001"}]
rc = StrictRedisCluster(startup_nodes=nodes, decode_responses=True)

@app.route('/<sekolah_id>', methods=['GET'])
def getBySekolahId(sekolah_id):
	print 'Getting data from redis : SMP '+sekolah_id+'....'
	pattern = 'sekolah:'+sekolah_id+':*'
	keys = rc.keys(pattern)
	res = []
	for key in keys:
		ret_data = rc.hgetall(key)
		res.append(ret_data)
	response = app.response_class(
        response= dumps(res),
        status=200,
        mimetype='application/json'
    )
	return response

@app.route('/<sekolah_id>', methods=['POST'])
def storeBatch(sekolah_id):
	print 'Redis caching for : SMP '+sekolah_id+'....'
	data = request.get_json()[0]
	data = loads(data)
	for item in data:
		pattern = 'sekolah:'+sekolah_id+':'+str(item['rank'])
		ins_data = {
			'rank' : item['rank'],
			'no_un' : item['no_un'],
			'nama' : item['nama'],
			'bind' : item['bind'],
			'big' : item['big'],
			'mat' : item['mat'],
			'ipa' : item['ipa'],
			'total' : item['total'],
			'pilihan1' : item['pilihan1'],
			'pilihan2' : item['pilihan2']
		}
		ins = rc.hmset(pattern, ins_data)
	return dumps(data)

@app.route('/flush', methods=['POST'])
def flushCache():
	rc.flushdb()
	return dumps(True)

if __name__ == '__main__':
    app.run(debug=True)