if hash mysql 2>/dev/null; then
    echo 'MySQL Already Installed'
else
    sudo apt-get update
    cd /tmp
    curl -OL https://github.com/sysown/proxysql/releases/download/v1.4.4/proxysql_1.4.4-ubuntu16_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb

    sudo apt-get install libaio1
    sudo apt-get install libmecab2
    sudo dpkg -i proxysql_1.4.4-ubuntu16_amd64.deb
    sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
    sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb

    sudo apt-get install -y apache2 php libapache2-mod-php php-mcrypt php-mysql
    # cd /tmp
    # wget -c http://wordpress.org/latest.tar.gz
    # tar -xzvf latest.tar.gz
    # sudo rm /var/www/html/*
    # sudo mv wordpress/* /var/www/html
    # sudo chown -R www-data:www-data /var/www/html
    # sudo chmod -R 755 /var/www/html
    # sudo service apache2 restart

    sudo ufw allow 33061
    sudo ufw allow 3306

    sudo systemctl start proxysql
    #mysql -u admin -padmin -h 127.0.0.1 -P 6032 < /vagrant/mysql_files/proxysql.sql
fi
