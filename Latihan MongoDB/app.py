from flask import Flask, request
from pymongo import MongoClient
import json
from bson.json_util import loads, dumps, ObjectId

# pip install flask pymongo


client = MongoClient('mongodb+srv://dbmaster:bojokuayu@bdt2018-efrjb.gcp.mongodb.net/test?retryWrites=true')
db = client.digidb
digi = db.digidb
app = Flask(__name__)

# Get All
@app.route('/api/digi', methods=['GET'])
def findAll():
	cursor = digi.find({})
	output = []
	for item in cursor:
		output.append(item)
	return dumps(output)

# Get by ID
@app.route('/api/digi/<id>', methods=['GET'])
def findOne(id):
	if ObjectId.is_valid(id):
		digimon = [i for i in digi.find({"_id": ObjectId(id)})]
		return dumps(digimon)
	else:
		return 'Please provide a valid ObjectId'

# Create
@app.route('/api/digi', methods=['POST'])
def create():
	data = request.get_json(force=True)
	if isinstance(data, list):
		result = digi.insert_many(data)
		ins_id = result.inserted_ids
	else:
		result = digi.insert_one(data)
		ins_id = result.inserted_id
	return dumps(ins_id)

# Delete by ID
@app.route('/api/digi/<id>', methods=['DELETE'])
def delete(id):
	if ObjectId.is_valid(id):
		if digi.count_documents({"_id": ObjectId(id)}) > 0:
			result = digi.delete_one({"_id": ObjectId(id)})
			return dumps(result.modified_count)
		else:
			return 'ObjectId not found'
	else:
		return 'Please provide a valid ObjectId'

# Update by ID
@app.route('/api/digi/<id>', methods=['PUT'])
def update(id):
	if ObjectId.is_valid(id):
		if digi.count_documents({"_id": ObjectId(id)}) > 0:
			data = request.get_json(force=True)
			result = digi.update_one({"_id": ObjectId(id)}, {"$set": data})
			digimon = [i for i in digi.find({"_id": ObjectId(id)})]
			return dumps(digimon)
		else:
			return 'ObjectId not found'
	else:
		return 'Please provide a valid ObjectId'
		
if __name__ == '__main__':
	app.run(debug=True)