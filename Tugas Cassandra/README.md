# Tugas Cassandra

Kuliah Basis Data Terdistribusi 2018

## Implementasi Sistem
### Deskripsi Sistem
Sistem ini menggunakan vagrant, dengan Vagrantfile yang telah disediakan oleh dosen.

### Deskripsi dataset
Dataset yang digunakan adalah dataset [Ufo Sightings](https://www.kaggle.com/NUFORC/ufo-sightings/version/1), yaitu data mengenai laporan warga yang mengaku melihat UFO.
Dataset ini terdiri dari 11 kolom, yaitu sebagai berikut:

  1. Datetime
  2. City
  3. State
  4. Country
  5. Shape
  6. Durations (seconds)
  7. Durations (hours/min)
  8. Comments
  9. Date Posted
  10. Latitude
  11. Longitude

### Konfigurasi Cassandra Single Node

#### 1. Memodifikasi Vagrantfile

Sistem ini menggunakan Vagrantfile yang telah diberikan oleh dosen. Terdapat beberapa perubahan dalam Vagrantfile ini guna menyesuaikan dengan lingkungan host. Yaitu perubahan pertama: **Merubah bridge networking** menjadi adapter yang sesuai dengan lingkungan host, dalam hal ini adalah ``wl01``. Perubahan kedua adalah **mengurangi jummlah memori** menjadi **1024 MB**. Sehingga hasil akhir dari Vagrantfile adalah sebagai berikut:

```ruby
  Vagrant.configure("2") do |config|
   
    config.vm.box = "ubuntu/xenial64"
    config.vm.hostname = "cassandra"
    config.vm.network "private_network", ip: "192.168.33.200"
    config.vm.network "public_network", bridge: "wlo1"

    config.vm.provider "virtualbox" do |vb|
      # Display the VirtualBox GUI when booting the machine
      vb.name = "cassandra"
      vb.gui = false
      # Customize the amount of memory on the VM:
      vb.memory = "1024"
    end

    config.vm.provision "shell", path: "provision/default.sh", privileged: false
  end
```

Selanjutnya jalankan vagrant dengan menjalankan perintah di direktori Vagrantfile:
```sh
  $ vagrant up
```

#### 2. Memodifikasi Dataset

Langkah selanjutnya adalah memodifikasi dataset agar dapat dengan mudah diimport ke Cassandra.

- Menambahkan primary key

Yang pertama harus dimodifikasi adalah menambahkan primary key. Karena pada dataset UFO Sightings tidak ada kolom yang dapat direpresentasikan menjadi primary key. Kolom yang akan dijadikan primary key adalah kolom baru bernama ``id`` yang berisi integer incremental. Untuk menambahkan kolom ini, digunakan Libre Office Calc agar lebih mudah.

- Mengurutkan nama kolom berdasarkan abjad

Selanjutnya adalah mengurutkan kolom berdasarkan abjad, ascending dari kiri ke kanan. Ini juga diperlukan untuk mempermudah proses import ke dalam Apache Cassandra. Tool yang digunakan masih tetap sama, yaitu Libre Office Calc

#### 3. Mengimport Dataset

Setelah modifikasi pada dataset, selanjutnya adalah mengimpor dataset ke Apache Cassandra. Namun sebelum itu, terlebih dahulu harus membuat database dan table.

- SSH ke Cassandra Node
```sh
  $ vagrant ssh
```

- Masuk ke Cassandra Query Language Shell (cqlsh)
```sh
  (vagrant) $ cqlsh
```

- Buat sebuah database (keyspace)
```sql
  (vagrant) cqlsh> CREATE KEYSPACE IF NOT EXISTS ufo WITH REPLICATION = {'class': 'NetworkTopologyStrategy', 'datacenter1': 3};
```

- Buat sebuah tabel
```sql
  (vagrant) cqlsh> CREATE TABLE ufo.sightings (id INT PRIMARY KEY, datetime TEXT, city TEXT, state TEXT, country TEXT, shape TEXT, duration float, duration_min TEXT, comments TEXT,latitude FLOAT, longitude FLOAT, date_posted TEXT);
```

- Impor dataset
```sql
  (vagrant) cqlsh> COPY ufo.sightings FROM 'complete_modified.csv' WITH DELIMITER=',' AND HEADER=TRUE;
```

Proses impor dataset telah selesai.

### Membuat Aplikasi CRUD
Aplikasi CRUD yang digunakan untuk menglah data UFO Sightings ini menggunakan bahasa pemrograman Python2.7. Aplikasi ini berjalan diatas kerangka kerja Flask. Karena itu, pertama-tama Flask harus diinstal di Cassandra Server, begitu juga dengan driver cassandra nya

- Menginstal Flask dan dependency nya.
```sh
  (vagrant) $ sudo apt-get install python-pip
  (vagrant) $ sudo pip install flask cassandra-driver
```

- Buat sebuah file server.py

```py
  from flask import Flask, request, Response, jsonify
  from cassandra.cluster import Cluster
  import json

  cluster = Cluster()
  session = cluster.connect()
  session.set_keyspace('ufo')
  app = Flask(__name__)

  #Read All
  @app.route('/', methods=['GET'])
  def findAll():
    rows = session.execute('SELECT json * from sightings');
    output = []
    for row in rows:
      output.append(row[0])

    out_cleaned = json.dumps(output).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
    return Response(out_cleaned, mimetype='application/json')

  #Read One
  @app.route('/<id>', methods=['GET'])
  def findOne(id):
    row = session.execute('SELECT json * from sightings WHERE id = '+id);
    if row:
      out_cleaned = json.dumps(row[0]).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
      return Response(out_cleaned, mimetype='application/json')
    else:
      return 'The id='+id+' is not found'

  # #Create
  @app.route('/', methods=['POST'])
  def create():
    data = request.get_json(force=True)[0]
    print data
    try:
      query = session.execute(
        """
        INSERT INTO sightings (id, city, comments, country, date_posted, datetime, duration, duration_min, latitude, longitude, shape, state) 
        VALUES (%(id)s, %(city)s, %(comments)s, %(country)s, %(date_posted)s, %(datetime)s, %(duration)s, %(duration_min)s, %(latitude)s, 
        %(longitude)s, %(shape)s, %(state)s)
        """, data)
      out_cleaned = json.dumps(query).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
      return Response(out_cleaned, mimetype='application/json')
      pass
    except Exception as e:
      print e
      return 'Insert error'

  # #Update
  @app.route('/<id>', methods=['PATCH'])
  def updateOne(id):
    data = request.get_json(force=True)[0]
    print data
    try:
      query = session.execute(
        """
        UPDATE sightings SET city = %(city)s, latitude = %(latitude)s, country = %(country)s, state = %(state)s, comments = %(comments)s, 
        datetime = %(datetime)s, duration = %(duration)s, shape = %(shape)s, longitude = %(longitude)s, duration_min = %(duration_min)s,
        date_posted = %(date_posted)s WHERE id = 
        """+id, data)
      return findOne(id)
      pass
    except Exception as e:
      print e
      return 'Update error'


  # #Delete
  @app.route('/<id>', methods=['DELETE'])
  def deleteOne(id):
    row = session.execute('DELETE FROM sightings WHERE id = '+id);
    print row
    return 'Delete Successful'


  if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
```

File ini akan menjalankan sebuah service yang dapat melayani CRUD. Service ini berjalan pada IP cassandra node di port 5000 (``http://192.168.33.200:5000``). Berikut penjelasan dari setiap service yang ada:

- Read
  - URI : ``/``
  - Method : GET
  - Return Value : Semua row dalam keyspace ``ufo`` tabel ``sightings``

  ![Contoh Read](images/read.png) 

- Create
  - URI : ``/``
  - Method : POST
  - Parameter : Nama kolom dan value yang akan diinsert
  - Return Value : Row yang di create / 'Create Failed'

  ![Contoh Create](images/create.png) 

- Find One
  - URI : ``/{id}``
  - Method : GET
  - Parameter : ID yang akan dicari
  - Return Value : Row yang dicari / 'The id={id} does not exists'

  ![Contoh Find](images/find.png)

- Update
  - URI : ``/{id}``
  - Method : PATCH
  - Parameter : Nama kolom dan value yang akan diupdate dan ID row
  - Return Value : Row yang telah diupdate

  ![Contoh Update](images/update.png) 

- DELETE
  - URI : ``/{id}``
  - Method : DELETE
  - Parameter : ID row
  - Return Value : 'Delete Success' / 'Delete Failed'

  ![Contoh Delete](images/delete.png) 