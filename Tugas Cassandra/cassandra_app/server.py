from flask import Flask, request, Response, jsonify
from cassandra.cluster import Cluster
import json

cluster = Cluster()
session = cluster.connect()
session.set_keyspace('ufo')
app = Flask(__name__)

#Read All
@app.route('/', methods=['GET'])
def findAll():
	rows = session.execute('SELECT json * from sightings');
	output = []
	for row in rows:
		output.append(row[0])

	out_cleaned = json.dumps(output).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
	return Response(out_cleaned, mimetype='application/json')

#Read One
@app.route('/<id>', methods=['GET'])
def findOne(id):
	row = session.execute('SELECT json * from sightings WHERE id = '+id)
	if row:
		out_cleaned = json.dumps(row[0]).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
		return Response(out_cleaned, mimetype='application/json')
	else:
		return 'The id='+id+' is not found'

# #Create
@app.route('/', methods=['POST'])
def create():
	data = request.get_json(force=True)[0]
	print data
	try:
		query = session.execute(
			"""
			INSERT INTO sightings (id, city, comments, country, date_posted, datetime, duration, duration_min, latitude, longitude, shape, state) 
			VALUES (%(id)s, %(city)s, %(comments)s, %(country)s, %(date_posted)s, %(datetime)s, %(duration)s, %(duration_min)s, %(latitude)s, 
			%(longitude)s, %(shape)s, %(state)s)
			""", data)
		out_cleaned = json.dumps(query[0]).replace('"{', '{').replace('\\"', '"').replace('}"', '}')
		return Response(out_cleaned, mimetype='application/json')
		pass
	except Exception as e:
		print e
		return 'Insert error'

# #Update
@app.route('/<id>', methods=['PATCH'])
def updateOne(id):
	data = request.get_json(force=True)[0]
	print data
	try:
		query = session.execute(
			"""
			UPDATE sightings SET city = %(city)s, latitude = %(latitude)s, country = %(country)s, state = %(state)s, comments = %(comments)s, 
			datetime = %(datetime)s, duration = %(duration)s, shape = %(shape)s, longitude = %(longitude)s, duration_min = %(duration_min)s,
			date_posted = %(date_posted)s WHERE id = 
			"""+id, data)
		return findOne(id)
		pass
	except Exception as e:
		print e
		return 'Update error'


# #Delete
@app.route('/<id>', methods=['DELETE'])
def deleteOne(id):
	row = session.execute('DELETE FROM sightings WHERE id = '+id);
	print row
	return 'Delete Successful'


if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)