#!/usr/bin/python
# read in.csv adding one column for UUID

import csv
import uuid
from datetime import datetime

fin = open('complete_withuuid.csv', 'rb')
fout = open('complete_withuuid_datatypefix.csv', 'w')

reader = csv.reader(fin, delimiter=',', quotechar='"')
writer = csv.writer(fout, delimiter=',', quotechar='"')

firstrow = True
for row in reader:
    if firstrow:
        row.append('UUID')
        firstrow = False
    else:
        row.append(uuid.uuid4())
        row[4] = row[4].replace('/', '-')
        row[4] = datetime.strptime(row[4], "%m-%d-%Y %H:%M").strftime("%Y-%m-%d %H:%M")
    writer.writerow(row)
