# Tugas 2 Partisi Basis Data

Kuliah Basis Data Terdistribusi 2018

## Outline
- [Deskripsi Server](#deskripsi-server)
- [Implementasi Paritisi 1: **Sakila** DB](#implementasi-partisi-1-sakila-db)
  - [Deskripsi dataset](#deskripsi-dataset)
  - [Proses pembuatan Partisi](#proses-pembuatan-partisi)
    - [Partisi Tabel Payment](#partisi-tabel-payment)
    - [Benchmarking Tabel Payment](#benchmarking)
    - [Partisi Tabel Rental](#partisi-tabel-rental)
    - [Benchmarking Tabel Rental](#benchmarking-1)
- [Implementasi Partisi 2 : **Measures** dataset](#implementasi-partisi-2-measures-dataset)
  - [Deskripsi dataset](#deskripsi-dataset-2)
  - [Import dataset](#import-dataset)
  - [Benchmarking](#benchmarking-2)

## Deskripsi Server
- Sistem operasi : Linux Ubuntu 18.04.01
- Versi MySQL : MySQL 5.7.23
- RAM : 8GB
- CPU : 6 Cores

## Implementasi Partisi 1: Sakila DB
### Deskripsi dataset
Dataset ini terdiri dari 16 tabel + 7 View. Masing-masing tabel memiliki jumlah baris data sebagai berikut:

![table list](images/2018/09/Screenshot from 2018-09-25 10-13-57.png)

Pada gambar di atas, view adalah row yang memiliki TABLE_ROWS = NULL

### Proses Pembuatan Partisi
Secara teoritis, partisi pada basis data akan menyebabkan operasi query lebih cepat dan *reliable*. Untuk memaksimalkan kelebihan itu, tabel yang akan dipartisi sebaiknya adalah tabel yang sifatnya transaksional dan sering berubah-ubah datanya. Dalam hal ini kita mengambil contoh tabel ***payment*** dan ***rental***.

### Partisi Tabel Payment
##### Penentuan *predicate*
- *p<sub>0</sub>* : Data dengan payment_date sebelum tahun 2005
- *p<sub>1</sub>* : Data dengan payment_date tahun 2005 - 2006
- *p<sub>2</sub>* : Data dengan payment_date setelah tahun 2006

#### Jenis partisi
Jenis partisi yang digunakan pada contoh ini adalah RANGE dengan parameter ***payment_date***

Sehingga berdasarkan syarat/predikat tersebut tabel akan dipartisi menjadi 3 bagian: yaitu partisi ***p<sub>0</sub>*** , ***p<sub>1</sub>*** , dan ***p<sub>2</sub>***

#### Implementasi Partisi
Sebelum menjadikan tabel ini menjadi partisi, tabel harus dipersiapkan terlebih dahulu
1.  Drop semua foreign key dan indexnya

  ```sql
  ALTER TABLE `sakila`.`payment`
  DROP FOREIGN KEY `fk_payment_staff`,
  DROP FOREIGN KEY `fk_payment_rental`,
  DROP FOREIGN KEY `fk_payment_customer`;
  ALTER TABLE `sakila`.`payment`
  DROP INDEX `fk_payment_rental` ,
  DROP INDEX `idx_fk_customer_id` ,
  DROP INDEX `idx_fk_staff_id` ;
  ```

2. Tambahkan ***payment_date*** sebagai primary key

  ```sql
  ALTER TABLE `sakila`.`payment`
  DROP PRIMARY KEY,
  ADD PRIMARY KEY (`payment_id`, `payment_date`);
  ```

3. Drop trigger yang terdapat pada ***payment_date*** (untuk keperluan benchmarking)

  ```sql
  USE `sakila`;
  DELIMITER $$
  USE `sakila`$$
  DROP TRIGGER IF EXISTS `sakila`.`payment_date` $$
  DELIMITER ;
  ```

Selanjutnya adalah mempartisi tabel ***payment*** dengan command :

```sql
  ALTER TABLE `sakila`.`payment`
  PARTITION BY RANGE (YEAR(payment_date)) (
  PARTITION p0 VALUES LESS THAN (2005),
  PARTITION p1 VALUES LESS THAN (2006),
  PARTITION p2 VALUES LESS THAN (MAXVALUE));
```

#### Benchmarking
Untuk memverifikasi apakah partisi telah berhasil dilakukan, dapat menguji dengan cara memasukkan data baru dan mencoba men-selectnya dari partisi.
```sql
-- Expected p0 partition data :
INSERT INTO payment VALUES (16050, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16051, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16052, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16053, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16054, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16055, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16056, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16057, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16058, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16059, 599, 2, 15725, 3.00, '2004-01-01 11:25:00', '2004-01-01 11:25:00');

-- Expected p1 partition data :
INSERT INTO payment VALUES (16060, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16061, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16062, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16063, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16064, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16065, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16066, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16067, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16068, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16069, 599, 2, 15725, 3.00, '2005-02-02 11:25:00', '2004-01-01 11:25:00');

-- Expected p2 partition data :
INSERT INTO payment VALUES (16070, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16071, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16072, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16073, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16074, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16075, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16076, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16077, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16078, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
INSERT INTO payment VALUES (16079, 599, 2, 15725, 3.00, '2007-02-02 11:25:00', '2004-01-01 11:25:00');
```

Lalu coba lakukan query select di partisi yang benar:
```sql
  SELECT * FROM payment PARTITION (p0) WHERE payment_id = 16051;
```
![insert p0](images/2018/09/Screenshot from 2018-09-27 16-34-21.png)

Dan mengujinya di partisi yang salah:
```sql
  SELECT * FROM payment PARTITION (p1) WHERE payment_id = 16051;
  SELECT * FROM payment PARTITION (p2) WHERE payment_id = 16051;
```

![insert p1](images/2018/09/Screenshot from 2018-09-25 10-39-26.png)

Terbukti bahwa pada data dengan ***payment_date*** sebelum tahun 2005 akan masuk ke partisi ***p<sub>0</sub>***

### Partisi Tabel Rental
##### Penentuan *predicate*
- *p<sub>0</sub>* : Data dengan (rental_id mod 3) = 0
- *p<sub>1</sub>* : Data dengan (rental_id mod 3) = 1
- *p<sub>2</sub>* : Data dengan (rental_id mod 3) = 2

#### Jenis partisi
Jenis partisi yang digunakan pada contoh ini adalah HASH dengan parameter ***rental_id*** dan jumlah partisi 3

Sehingga berdasarkan syarat/predikat tersebut tabel akan dipartisi menjadi 3 bagian: yaitu partisi ***p<sub>0</sub>*** , ***p<sub>1</sub>*** , dan ***p<sub>2</sub>***

#### Implementasi Partisi
Sebelum menjadikan tabel ini menjadi partisi, tabel harus dipersiapkan terlebih dahulu
1.  Drop semua foreign key dan indexnya

  ```sql
  ALTER TABLE `sakila`.`rental`
  DROP FOREIGN KEY `fk_rental_staff`,
  DROP FOREIGN KEY `fk_rental_inventory`,
  DROP FOREIGN KEY `fk_rental_customer`;
  ALTER TABLE `sakila`.`rental`
  DROP INDEX `idx_fk_staff_id` ,
  DROP INDEX `idx_fk_customer_id` ,
  DROP INDEX `idx_fk_inventory_id` ;
  ```

2. Tambahkan ***rental_id*** sebagai unique dengan catatan harus dalam unique index yang sama dengan kolom unique yang lainnya. Bisa dilakukan dengan syntax berikut

  ```sql
  ALTER TABLE `sakila`.`rental`
  DROP INDEX `rental_date` ,
  ADD UNIQUE INDEX `rental_date` (`rental_id` ASC, `rental_date` ASC, `inventory_id` ASC, `customer_id` ASC);
  ```

Selanjutnya adalah mempartisi tabel ***rental*** dengan command :

```sql
ALTER TABLE `sakila`.`rental`
PARTITION BY HASH (rental_id)
PARTITIONS 3;
```

#### Benchmarking
Untuk memverifikasi apakah partisi telah berhasil dilakukan, dapat menguji dengan cara memasukkan data baru dan mencoba men-selectnya dari partisi.
```sql
INSERT INTO rental VALUES ('16050', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16051', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16052', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16053', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16054', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16055', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16056', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16057', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16058', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16059', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');

INSERT INTO rental VALUES ('16060', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16061', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16062', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16063', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16064', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16065', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16066', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16067', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16068', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16069', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');

INSERT INTO rental VALUES ('16070', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16071', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16072', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16073', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16074', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16075', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16076', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16077', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16078', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
INSERT INTO rental VALUES ('16079', '2005-08-23 22:50:12', '2666', '393', '2005-08-30 01:01:12', '2', '2006-02-15 21:30:53');
```

Lalu coba lakukan query select di partisi yang benar. Misalkan kita akan mencoba mendapatkan data **rental_id = 16051**. Dimana **16051 modulus 3 = 1**. Maka seharusnya data ini ada di partisi ***p<sub>1</sub>***
```sql
  SELECT * FROM rental PARTITION (p1) WHERE rental_id = 16051;
```
![rental benar](images/2018/09/Screenshot from 2018-09-27 16-56-41.png)

Dan mengujinya di partisi yang salah:
```sql
  SELECT * FROM rental PARTITION (p0) WHERE rental_id = 16051;
  SELECT * FROM rental PARTITION (p2) WHERE rental_id = 16051;
```

![rental salah](images/2018/09/Screenshot from 2018-09-27 16-57-01.png)

Terbukti bahwa pada data dengan fungsi hash ***rental_id mod 3*** telah masuk ke partisi yang sesuai

## Implementasi Partisi 2: Measures Dataset
### Deskripsi Dataset
Dataset ini terdiri dari 2 tabel, yaitu ***measures*** dan ***partitioned_measures***

Seperti namanya, tabel ini identik. Namun Tabel ***partitioned_measures*** sudah dipartisi dengan Horizontal Partition pada kolom ***measure_timestamp***. Setiap tabel berisi 1,8 juta record yang identik
  - Sumber dataset : [Vertabelo](http://www.vertabelo.com/blog/technical-articles/everything-you-need-to-know-about-mysql-partitions)

### Import Dataset
- Download ***sample_1_8_M_rows_data.zip***
- Ekstrak, akan terdapat file ***sample_1_8_M_rows_data.sql*** di dalamnya
- Buka terminal di direktori tempat file tersebut disimpan
- Masuk ke MySQL : ``mysql -u root -p``
- Buat database bernama *measures* : ``create database measures; use measures;``
- Impor file .sql nya : ``source /path/to/sample_1_8_M_rows_data.sql`` (full path)

### Benchmarking
Benchmarking dilakukan dengan 2 tipe : *SELECT* dan *BIG DELETE*. Masing-masing dilakukan 10 kali, lalu kecepatan querynya dicatat untuk kemudian di rata-rata. Berikut adalah hasilnya

#### Select Benchmark
![select benchmark](images/2018/09/Screenshot from 2018-09-25 13-52-27.png)

Tanpa Partisi
![tanpa partisi](images/2018/09/Screenshot from 2018-09-24 20-17-36.png)

Dengan Partisi
![partisi select](/images/2018/09/Screenshot from 2018-09-24 20-17-51.png)

#### Big Delete Benchmark (85314 rows)
![delete benchmark](images/2018/09/Screenshot from 2018-09-25 13-53-09.png)

Tanpa Partisi
![tanpa partisi delete](/images/2018/09/Screenshot from 2018-09-24 21-25-18.png)

Dengan Partisi
![dengan partisi image](/images/2018/09/Screenshot from 2018-09-24 21-25-29.png)
