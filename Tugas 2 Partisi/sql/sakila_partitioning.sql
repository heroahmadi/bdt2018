ALTER TABLE `sakila`.`payment`
DROP FOREIGN KEY `fk_payment_staff`,
DROP FOREIGN KEY `fk_payment_rental`,
DROP FOREIGN KEY `fk_payment_customer`;
ALTER TABLE `sakila`.`payment`
DROP INDEX `fk_payment_rental` ,
DROP INDEX `idx_fk_customer_id` ,
DROP INDEX `idx_fk_staff_id` ;

ALTER TABLE `sakila`.`payment`
DROP PRIMARY KEY,
ADD PRIMARY KEY (`payment_id`, `payment_date`);

USE `sakila`;
DELIMITER $$
USE `sakila`$$
DROP TRIGGER IF EXISTS `sakila`.`payment_date` $$
DELIMITER ;

ALTER TABLE `payment`
 PARTITION BY RANGE (YEAR(payment_date)) (
    PARTITION p0 VALUES LESS THAN (2005),
    PARTITION p1 VALUES LESS THAN (2006),
    PARTITION p2 VALUES LESS THAN (MAXVALUE)
);
