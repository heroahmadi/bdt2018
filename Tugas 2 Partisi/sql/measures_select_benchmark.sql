SELECT SQL_NO_CACHE
    COUNT(*)
FROM
    measures.measures
WHERE
    measure_timestamp >= '2016-01-01'
        AND DAYOFWEEK(measure_timestamp) = 1;
     
SELECT SQL_NO_CACHE
    COUNT(*)
FROM
    measures.partitioned_measures
WHERE
    measure_timestamp >= '2016-01-01'
        AND DAYOFWEEK(measure_timestamp) = 1;