SET SQL_SAFE_UPDATES = 0;

DELETE
FROM measures.measures
WHERE  measure_timestamp < '2015-01-01';

ALTER TABLE measures.partitioned_measures 
DROP PARTITION to_delete_logs ;
