ALTER TABLE `sakila`.`rental`
DROP FOREIGN KEY `fk_rental_staff`,
DROP FOREIGN KEY `fk_rental_inventory`,
DROP FOREIGN KEY `fk_rental_customer`;
ALTER TABLE `sakila`.`rental`
DROP INDEX `idx_fk_staff_id` ,
DROP INDEX `idx_fk_customer_id` ,
DROP INDEX `idx_fk_inventory_id` ;

ALTER TABLE `sakila`.`rental`
DROP INDEX `rental_date` ,
ADD UNIQUE INDEX `rental_date` (`rental_id` ASC, `rental_date` ASC, `inventory_id` ASC, `customer_id` ASC);

ALTER TABLE `sakila`.`rental`
PARTITION BY HASH (rental_id)
PARTITIONS 3;