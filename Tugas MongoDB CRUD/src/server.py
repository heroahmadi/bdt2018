from flask import Flask, request
from mongoengine import *
from pymongo import UpdateOne
from bson.json_util import loads, dumps, ObjectId

class DigimonList(Document):
	Number = DecimalField()
	Digimon = StringField()
	Stage = StringField()
	Type = StringField()
	Attribute = StringField()
	Memory = DecimalField()
	EquipSlots = DecimalField()
	Lv50HP = DecimalField()
	Lv50SP = DecimalField()
	Lv50Atk = DecimalField()
	Lv50Def = DecimalField()
	Lv50Int = DecimalField()
	Lv50Spd = DecimalField()
	meta = {'collection': 'digimonlist'}

	def do_update(self, obj):
		self.update(
			set__Number = obj['Number'],
			set__Digimon = obj['Digimon'],
			set__Stage = obj['Stage'],
			set__Type = obj['Type'],
			set__Attribute = obj['Attribute'],
			set__Memory = obj['Memory'],
			set__EquipSlots = obj['EquipSlots'],
			set__Lv50HP = obj['Lv50HP'],
			set__Lv50SP = obj['Lv50SP'],
			set__Lv50Atk = obj['Lv50Atk'],
			set__Lv50Def = obj['Lv50Def'],
			set__Lv50Int = obj['Lv50Int'],
			set__Lv50Spd = obj['Lv50Spd']
		)

	def do_create(self, obj):
		self.Number = obj['Number']
		self.Digimon = str(obj['Digimon'])
		self.Stage = str(obj['Stage'])
		self.Type = str(obj['Type'])
		self.Attribute = str(obj['Attribute'])
		self.Memory = obj['Memory']
		self.EquipSlots = obj['EquipSlots']
		self.Lv50HP = obj['Lv50HP']
		self.Lv50SP = obj['Lv50SP']
		self.Lv50Atk = obj['Lv50Atk']
		self.Lv50Def = obj['Lv50Def']
		self.Lv50Int = obj['Lv50Int']
		self.Lv50Spd = obj['Lv50Spd']

class MoveList(Document):
	Move = StringField()
	SPCost = DecimalField(db_field='SP Cost')
	Type = StringField()
	Power = DecimalField()
	Attribute = StringField()
	Inheritable = StringField()
	Description = StringField()
	meta = {'collection': 'movelist'}

	def do_update(self, obj):
		self.update(
			set__Move = obj['Move'],
			set__SPCost = obj['SP Cost'],
			set__Type = obj['Type'],
			set__Power = obj['Power'],
			set__Attribute = obj['Attribute'],
			set__Inheritable = obj['Inheritable'],
			set__Description = obj['Description']
		)

	def do_create(self, obj):
		self.Move = obj['Move']
		self.SPCost = str(obj['SP Cost'])
		self.Type = str(obj['Type'])
		self.Attribute = str(obj['Attribute'])
		self.Power = obj['Power']
		self.Inheritable = obj['Inheritable']
		self.Description = obj['Description']

class SupportList(Document):
	Name = StringField()
	Description = StringField()
	meta = {'collection': 'supportlist'}

	def do_update(self, obj):
		self.update(
			set__Name = obj['Name'],
			set__Description = obj['Description']
		)

	def do_create(self, obj):
		self.Name = obj['Name']
		self.Description = str(obj['Description'])

connect(host='192.168.33.10', port=27017, db='digidb', replicaset='bdt')
app = Flask(__name__)	

def GetModel(CollectionName):
	return globals()[CollectionName]


#Read All
@app.route('/<collection>', methods=['GET'])
def findAll(collection):
	Model = GetModel(collection)
	output = []
	for item in Model.objects:
		output.append(dict(item.to_mongo()))
	return dumps(output)

#Read One
@app.route('/<collection>/<id>', methods=['GET'])
def findOne(collection, id):
	Model = GetModel(collection)
	try:
		output = Model.objects.get(id=id)
		pass
	except DoesNotExist as e:
		return 'The ObjectId doest not exists'

	return dumps(dict(output.to_mongo()))

#Create
@app.route('/<collection>', methods=['POST'])
def create(collection):
	Model = GetModel(collection)
	data = request.get_json(force=True)[0]
	obj = Model()
	obj.do_create(data)
	obj.save()
	return dumps(dict(obj.to_mongo()))

#Update
@app.route('/<collection>/edit/<id>', methods=['POST'])
def updateOne(collection, id):
	Model = GetModel(collection)
	try:
		output = Model.objects.get(id=id)
		data = request.get_json(force=True)[0]
		output.do_update(data)

		output.reload()
		return dumps(dict(output.to_mongo()))
		pass
	except DoesNotExist as e:
		return 'The ObjectId doest not exists'


#Delete
@app.route('/<collection>/delete/<id>', methods=['GET'])
def deleteOne(collection, id):
	Model = GetModel(collection)
	try:
		output = Model.objects.get(id=id)
		output.delete()

		return "Delete Success"
		pass
	except DoesNotExist as e:
		return 'The ObjectId doest not exists'


if __name__ == '__main__':
	app.run(host='0.0.0.0', debug=True)