# Tugas Demo ETS MySQL Replication

Kuliah Basis Data Terdistribusi 2018

## Outline
- [Tujuan](#tujuan)
- [Deskripsi Sistem](#deskripsi-sistem)
- [Implementasi Sistem](#implementasi-sistem)
- [Testing](#testing-dengan-mematikan-salah-satu-node)

## Tujuan
- Membuat server basis data terdistribusi dengan menggunakan konsep group replication
- Mampu menambahkan load balancer (ProxySQL) untuk membagi request ke server basis data
- Menambahkan aplikasi CMS (Wordpress) yang memanfaatkan arsitektur tersebut
- Menguji kehandalan sistem (testing) dengan menyimulasikan matinya beberapa node dan menunjukkan bahwa data tetap tereplikasi pada node-node server basis data.


## Deskripsi Sistem

### Arsitektur Sistem
![arsitektur](images/2018/10/arsitektur.jpg)

Sistem ini dibangun dengan virtualisasi. Dan untuk memudahkan, sistem ini dibangun dengan bantuan **Vagrant**. Seperti yang ada pada gambar arsitektur sistem, lingkungan sistem ini terdiri dari ProxySQL Server dan MySQL Group Replication Server.

### ProxySQL Server
ProxySQL Server bertindak sebagai load balancer. Server ini mempunyai konfigurasi seperti berikut:
- Sistem Operasi: Ubuntu 16.04
- MySQL Server Community Edition
- 512 MB RAM

Server mempunyai IP lokal yaitu ```192.168.33.10```

### Group Replication Servers/Cluster
Cluster ini terdiri dari 3 server, yaitu ***db1***, ***db2***, dan ***db3***. Setiap server mempunyai konfigurasi seperti berikut:
- Sistem Operasi: Ubuntu 16.04
- MySQL Server Community Edition
- 512 MB RAM

Masing-masing server mempunyai IP lokal yaitu ```192.168.33.11``` (***db1***), ```192.168.33.12``` (***db2***), dan ```192.168.33.13``` (***db3***)

## Implementasi Sistem
Implementasi pada sistem ini berdasarkan dari referensi yang telah diberikan dosen, yaitu berupa file ```mysql-cluster-proxysql.zip```. Berikut adalah langkah-langkah implementasi sistem:

### Step 1 - Ekstrak ZIP referensi
Pertama-tama, ekstrak file ```mysql-cluster-proxysql.zip``` kedalam sebuah direktori. Lalu buka direktori tersebut di dalam terminal.

```bash
$ cd mysql-cluster-proxysql/
```

### Step 2 - Modifikasi Vagrantfile
Dengan menggunakan text editor, ubah **Vagrantfile** menjadi seperti berikut:

```ruby
# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.

Vagrant.configure("2") do |config|

  # MySQL Cluster dengan 3 node
  (1..3).each do |i|
    config.vm.define "db#{i}" do |node|
      node.vm.hostname = "db#{i}"
      node.vm.box = "bento/ubuntu-16.04"
      node.vm.network "private_network", ip: "192.168.33.1#{i}"

      # Opsional. Edit sesuai dengan nama network adapter di komputer
      node.vm.network "public_network", bridge: "wlo1"

      node.vm.provider "virtualbox" do |vb|
        vb.name = "db#{i}"
        vb.gui = false
        vb.memory = "512"
      end

      node.vm.provision "shell", path: "deployMySQL1#{i}.sh", privileged: false
    end
  end

  config.vm.define "proxy" do |proxy|
    proxy.vm.hostname = "proxy"
    proxy.vm.box = "bento/ubuntu-16.04"
    proxy.vm.network "private_network", ip: "192.168.33.10"
    proxy.vm.network "public_network",  bridge: "wlo1"

    proxy.vm.provider "virtualbox" do |vb|
      vb.name = "proxy"
      vb.gui = false
      vb.memory = "512"
    end

    proxy.vm.provision "shell", path: "deployProxySQL.sh", privileged: false
  end
end
```
Pada **Vagrantfile** ini dilakukan 3 perubahan. Yang **pertama** yaitu mengurangi memori yang dialokasikan untuk setiap VM, dari 1024MB menjadi 512MB. Perubahan ini dilakukan agar dapat menyesuaikan dengan lingkungan host. Perubahan **kedua** yaitu menyesuaikan network adapter yang digunakan, dalam hal ini adalah ***wlo1***. List network adapter dapat dilihat dengan menggunakan perintah:

```bash
$ ip a
```

### Step 2 - Provisioning
- ProxySQL server provisioning:

Pada step kedua ini, hanya provisioning untuk ProxySQL server saja yang dimodifikasi. Edit file ```deployProxySQL.sh``` dengan menggunakan text editor menjadi:


```bash
sudo apt-get update
cd /tmp
curl -OL https://github.com/sysown/proxysql/releases/download/v1.4.4/proxysql_1.4.4-ubuntu16_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb

sudo apt-get install libaio1
sudo apt-get install libmecab2
sudo dpkg -i proxysql_1.4.4-ubuntu16_amd64.deb
sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb

sudo ufw allow 33061
sudo ufw allow 3306

sudo systemctl start proxysql

# install apache, dan php
sudo apt-get install -y apache2 php libapache2-mod-php php-mcrypt php-mysql

# install wordpress
cd /tmp
wget -c http://wordpress.org/latest.tar.gz
tar -xzvf latest.tar.gz
sudo mv wordpress/* /var/www/html
sudo chown -R www-data:www-data /var/www/html
sudo chmod -R 755 /var/www/html
sudo service apache2 restart
```

Modifikasi yang dilakukan adalah menambahkan perintah untuk menginstall apache, php dan wordpress.

 - MySQL Cluster Server (***db1***) provisioning:

File ```deployMySQL11.sh```. Tidak ada perubahan
```bash
sudo apt-get update
sudo apt-get install libaio1
sudo apt-get install libmecab2
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo ufw allow 33061
sudo ufw allow 3306
sudo cp /vagrant/my11.cnf /etc/mysql/my.cnf
sudo service mysql restart
sudo mysql -u root -padmin < /vagrant/cluster_bootstrap.sql
sudo mysql -u root -padmin < /vagrant/addition_to_sys.sql
sudo mysql -u root -padmin < /vagrant/create_proxysql_user.sql
```

- MySQL Cluster Server (***db2***) provisioning:

File ```deployMySQL12.sh```. Tidak ada perubahan
```bash
sudo apt-get update
sudo apt-get install libaio1
sudo apt-get install libmecab2
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo ufw allow 33061
sudo ufw allow 3306
sudo cp /vagrant/my12.cnf /etc/mysql/my.cnf
sudo service mysql restart
sudo mysql -u root -padmin < /vagrant/cluster_member.sql
```

- MySQL Cluster Server (***db3***) provisioning:

File ```deployMySQL13.sh```. Tidak ada perubahan
```bash
sudo apt-get update
sudo apt-get install libaio1
sudo apt-get install libmecab2
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-common_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-client_5.7.23-1ubuntu16.04_amd64.deb
curl -OL https://dev.mysql.com/get/Downloads/MySQL-5.7/mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/root-pass password admin'
sudo debconf-set-selections <<< 'mysql-community-server mysql-community-server/re-root-pass password admin'
sudo dpkg -i mysql-common_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-client_5.7.23-1ubuntu16.04_amd64.deb
sudo dpkg -i mysql-community-server_5.7.23-1ubuntu16.04_amd64.deb
sudo ufw allow 33061
sudo ufw allow 3306
sudo cp /vagrant/my12.cnf /etc/mysql/my.cnf
sudo service mysql restart
sudo mysql -u root -padmin < /vagrant/cluster_member.sql
```

### Step 3 - Jalankan Vagrant
Konfigurasi awal vagrant sudah selesai. Selanjutnya jalankan vagrant dengan perintah:
```bash
$ vagrant up
```
Tunggu selama beberapa menit. Setelah itu cek apakah VM sudah berjalan dengan benar dengan perintah:
```bash
$ vagrant status
```
Akan muncul status seperti berikut:

![status](images/2018/10/Screenshot from 2018-10-18 20-30-06.png)

Selanjutnya jalankan provision tambahan untuk proxy SQL.

- SSH ke ProxySQL server
```bash
$ vagrant ssh proxy
```

- Jalankan:
```bash
(proxy)$ mysql -u admin -p -h 127.0.0.1 -P 6032 < /vagrant/proxysql.sql
#password: admin
```

- (opsional) Tes koneksi ke ProxySQL dari group MySQL. Dari salah satu node (di contoh ini ***db1***). Jalankan:
```bash
(db1)$ mysql -u playgrounduser -p -h 192.168.33.10 -P 6033
#password: playgroundpassword
```

### Step 3 - Konfigurasi Wordpress
Instalasi wordpress telah dimasukkan pada provisioning ProxySQL server. Langkah selanjutnya kita harus menyediakan database yang akan digunakan oleh Wordpress.

- Lakukan ```ssh``` ke ***db1***
```bash
$ vagrant ssh db1
```

- Masuk ke mysql
```bash
(db1)$ mysql -u root -p
#password: admin
```

- Buat database baru
```sql
(db1) mysql> CREATE DATABASE wordpress;
```

- Buat user baru untuk wordpress
```sql
(db1) mysql> CREATE USER 'wordpress'@'%' IDENTIFIED BY 'wordpress';
(db1) mysql> GRANT ALL PRIVILEGES ON wordpress.* TO 'wordpress'@'%';
(db1) mysql> FLUSH PRIVILEGES;
```
User baru sudah berhasil dibuat. Seperti yang telah diketahui, user dan database ini juga akan otomatis tereplikasi di MySQL cluster member, yaitu ***db2*** dan ***db3***. Selanjutnya, tambahkan user yang sama ke server ProxySQL. Dengan password yang sama juga.

- Di terminal baru, lakukan ```ssh``` ke ***proxy***
```bash
$ vagrant ssh proxy
```

- Masuk ke mysql
```bash
(proxy)$ mysql -u admin -p -h 127.0.0.1 -P 6032
#password: password
```

- Buat user baru untuk wordpress
```sql
(proxy) mysql> INSERT INTO mysql_users(username, password, default_hostgroup) VALUES ('wordpress', 'wordpress', 2);
(proxy) mysql> LOAD MYSQL USERS TO RUNTIME;
(proxy) mysql> SAVE MYSQL USERS TO DISK;
```

- Konfigurasi wordpress

Langkah selanjutnya adalah 'menyambungkan' aplikasi wordpress ke database yang telah kita buat. Caranya adalah mengunjungi halaman IP address proxy sql ([http://192.168.33.10](http://192.168.33.10)) pada browser. Akan muncul tampilan awal wordpress seperti berikut

![wordpress1](images/2018/10/Screenshot from 2018-10-18 16-46-37.png)

Klik ```continue```. Akan muncul halaman konfigurasi database wordpress. Isi sesuai konfigurasi yang telah dibuat sebelumnya. Arahkan ke **IP ProxySQL** untuk memfungsikan load balancernya.

![wordpress2](images/2018/10/Screenshot from 2018-10-18 16-50-48.png)

Klik ```submit```. Akan muncul halaman seperti berikut

![wordpress3](images/2018/10/Screenshot from 2018-10-18 16-51-09.png)

Lanjutkan proses instalasi dan isi form sesuai dengan keinginan anda

![wordpress4](images/2018/10/Screenshot from 2018-10-18 16-51-57.png)

![wordpress5](images/2018/10/Screenshot from 2018-10-18 16-52-22.png)

Konfigurasi wordpress telah berhasil.

![wordpress6](images/2018/10/Screenshot from 2018-10-18 16-52-39.png)

### Step 4 - Cek Replikasi
Instalasi pada wordpress telah sepenuhnya selesai. Selanjutnya cek apakah proses replikasi masih berjalan sebagaimana mestinya. Lakukan ```vagrant ssh [nama_vm]``` di ***db1***, ***db2***, dan ***db3***. Lalu coba show table di masing-masing VM

![cek1](images/2018/10/Screenshot from 2018-10-18 16-53-20.png)

![cek2](images/2018/10/Screenshot from 2018-10-18 16-53-48.png)

![cek3](images/2018/10/Screenshot from 2018-10-18 16-54-31.png)


## Testing Dengan Mematikan Salah Satu Node
Selanjutnya adalah melakukan testing apabila salah satu node mati. Dan memeriksa apakah proses sinkronisasi tetap berjalan sebagaimana mestinya. Berikut langkah-langkahnya

### Step 1 - Matikan Servis MySQL Pada Salah Satu Node
Dalam contoh ini, yang akan dimatikan adalah node ***db2***.

- SSH ke ***db2***
```bash
$ vagrant ssh db2
```

- Matikan servis MySQL
```bash
(db2)$ sudo service mysql stop
```

### Step 2 - Buat Post Baru di Wordpress
Selanjutnya adalah melakukan perubahan pada database selagi ***db2*** mati. Dalam contoh ini akan menambahkan sebuah post. Setelah login ke [http://192.168.33.10/wp-admin](http://192.168.33.10/wp-admin), buat post dengan konten bebas.

![postnew](images/2018/10/Screenshot from 2018-10-18 16-57-58.png)

### Step 3 - Hidupkan Kembali Node
Selanjutnya hidupkan kembali ***db2*** dengan menjalankan perintah:
```bash
(db2)$ sudo service mysql start
```

Kemudian cek apakah node ini berhasil melakukan sinkronisasi.

- Masuk ke MySQL
```bash
(db2)$ mysql -u wordpress -p
#password: wordpress
```

- Cek isi dari tabel wp_posts
```bash
(db2) mysql> use wordpress;
(db2) mysql> SELECT * FROM wp_posts\G;
```

![cekposts](images/2018/10/Screenshot from 2018-10-18 16-59-36.png)

Seperti yang dapat dilihat proses sinkronisasi telah berjalan sebagaimana mestinya. Dan ***db1***, ***db2***, dan ***db3*** memiliki isi database yang sama persis.
